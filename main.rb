require "redis"
require "highline/import"

username = ARGV[0]
channel = ARGV[1]

if !username
	username = ask "No username entered. Please type your username: "
end

if !channel
	channel = ask "Please type the channel you wish to enter: "
end

$redis = Redis.new(:host => "127.0.0.1", :port => 6379, :db => 15)
redis2 = Redis.new(:host => "127.0.0.1", :port => 6379, :db => 15)
t1 = Thread.new {
	$redis.subscribe(channel) do |on|
		on.message do |channel, message|
			puts "[#{username}] #{message}"
		end
	end
}

loop do
	message = ask " > "
	redis2.publish(channel, message)
end


